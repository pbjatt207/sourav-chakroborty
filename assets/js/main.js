$(function() {
	$("[rel='refresh']").on("click", function(e) {
		e.preventDefault();
		var target = $(this).attr("href");

		$(target).attr("src", "captcha?"+Date.now());
	});

	$("form.ajax-form").on("submit", function(e) {
		e.preventDefault();

		is_valid = $(this).is_valid();
	});
});